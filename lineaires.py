# -*- coding: utf-8 -*-
"""
Created on Mon Oct 14 14:58:14 2019

@author: g.peronato
"""

import os
import geopandas as gpd
import pandas as pd
import matplotlib.pyplot as plt


def getLineaires(networks,directory,scenario,geometrydir,Plot=False):
    lineaires = pd.DataFrame(columns = networks.Source.unique(), index=networks.DN.unique())
    networks["length"] = networks.geometry.length
    for Source in networks.Source.unique(): 
        lineaires.loc[:,Source] = networks[networks.Source == Source].groupby("DN").sum()["length"]
        lineaires = lineaires.sort_index()
        if Plot:
            buildings = gpd.read_file("{}/volumes_georef.gpkg".format(geometrydir))
            #DN
            fig, ax = plt.subplots() 
            buildings.plot(ax=ax,color="grey")
            networks[networks.Source==Source].plot(ax=ax,column="DN",legend=True, linewidth=2) #you can set here a vmax=
            plt.axis('off')
            plt.title("DN")
            plotdir = directory+"/"+"cartes"
            if not os.path.exists(plotdir):
                os.makedirs(plotdir)
            plt.savefig("{}/{}_{}.pdf".format(plotdir,scenario,Source))
            #Power
            fig, ax = plt.subplots() 
            buildings.plot(ax=ax,color="grey")
            networks[networks.Source==Source].plot(ax=ax,column="power",legend=True, linewidth=2) #you can set here a vmax=
            plt.axis('off')
            plt.title("Power [W]")
            plt.savefig("{}/{}_{}_power.pdf".format(plotdir,scenario,Source))            
            print("Plots saved in {}".format(plotdir))
    lineaires = lineaires.fillna(0)
    lineairesdir = directory+"/"+"lineaires"
    if not os.path.exists(lineairesdir):
        os.makedirs(lineairesdir)
    lineaires.to_csv("{}/{}.csv".format(lineairesdir,scenario))
    print("Lineaires saved in {}".format(lineairesdir))

# Code to debug or when used as standalone script   
if __name__ == "__main__":
    
    geometrydir = r"\\ter3ficw2k01\DATA\ECT_LC\Elioth\20_Projets\BAOA129 - Bruneseau Equipe Hardel\21 - SmartGrid\2 - Calc\data\reseau\geometrie\in\\"
    climelioth = r"S:\ECT_LC\Elioth\20_Projets\BAOA129 - Bruneseau Equipe Hardel\21 - SmartGrid\2 - Calc\data\consommation\besoins\climelioth\out\3\climelioth.parquet"
    outdir = r"\\ter3ficw2k01\DATA\ECT_LC\Elioth\20_Projets\BAOA129 - Bruneseau Equipe Hardel\21 - SmartGrid\2 - Calc\data\reseau\geometrie\out\\"
    DNdir = r"\\ter3ficw2k01\DATA\ECT_LC\Elioth\20_Projets\BAOA129 - Bruneseau Equipe Hardel\21 - SmartGrid\2 - Calc\data\reseau\DN\\"
    
    temperaturedir = r"S:\ECT_LC\Elioth\20_Projets\BAOA129 - Bruneseau Equipe Hardel\21 - SmartGrid\2 - Calc\data\reseau\temperatures\in\\"
    
    tempscenario = "cpcu-climespace"
    dimfactor = 1
    
    pathscenarios = os.listdir(geometrydir)
    
    Plot = True

    for scenario in pathscenarios:
        
        
        
        cmd = 'C:\ProgramData\Anaconda3\python run.py -g "{}" -n "{}" -c "{}" -d {} -o "{}"'.format(geometrydir + scenario,
                                                             temperaturedir + tempscenario+".json",
                                                            climelioth,
                                                            dimfactor,
                                                            outdir+tempscenario+"//"+scenario+".json")
        print(cmd)
        os.system(cmd)
        networks = gpd.read_file(outdir+tempscenario+"//"+scenario+".json")
        
        getLineaires(networks, DNdir+tempscenario, scenario, Plot)
        
