This module is used to size the pipes of a district heating&cooling network.

The output is a geoJSON file describing each network section as a line with the attributes:

- `DN` the nominal diameter of the pipe
- `power` the power used for the sizing

The geoJSON file also includes a non-spatial section with the same information (temperatures) as the JSON file that is used here as input.

By default, the sizing is computed on the max load for each building.


**Use**

    run.py -g <network_geometry_directory> -n <network_temperatures_json> -p <power> -d <dimfactor> -t <datetime> -o <output_file_path>



**Sample use**

     python run.py
     -g "S:\ECT_LC\Elioth\20_Projets\BAOA129 - Bruneseau Equipe Hardel\21 - SmartGrid\2 - Calc\data\reseau\geometrie\in\b2bsepare\0000-TRA_base"
     -n "S:\ECT_LC\Elioth\20_Projets\BAOA129 - Bruneseau Equipe Hardel\21 - SmartGrid\2 - Calc\data\reseau\temperatures\in\1\medium-temperature.json"
     -c "S:\ECT_LC\Elioth\20_Projets\BAOA129 - Bruneseau Equipe Hardel\21 - SmartGrid\2 - Calc\data\consommation\besoins\climelioth\out\3\climelioth.parquet"
     -o result.json



Optional parameters:

- `-d` to set a sizing factor: e.g `-d 2` to size using the double of the max power
- `-t` datetime of a specific hour to be used for sizing, e.g. `-t "2005-06-21 12:00"`, instead of the max power
