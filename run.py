# -*- coding: utf-8 -*-
"""
Created on Thu Oct 10 16:03:39 2019

@author: Giuseppe Peronato
"""
import network as nt
import sys
import json
import networkx as nx
import os
import pandas as pd
import geopandas as gpd
import getopt
import lineaires
import geosmartgrid
import numpy as np
script_path = os.path.abspath(__file__)
dir_path = os.path.dirname(script_path)

    
def main(argv):
    # Find the path of the script
    script_path = os.path.abspath(__file__)
    dir_path = os.path.dirname(script_path)
    sys.path.append(dir_path + "/python/")
    
    # Sample inputs
    
    path = ""
    datetime = ""
    #path = r"S:\ECT_LC\Elioth\20_Projets\BAOA129 - Bruneseau Equipe Hardel\21 - SmartGrid\2 - Calc\data\reseau\geometrie\in\b2bsepare\{}"
    path = r"S:\ECT_LC\Elioth\20_Projets\BAOA129 - Bruneseau Equipe Hardel\21 - SmartGrid\2 - Calc\data\reseau\geometrie\in\faisa\{}"
    
    #Climelioth
    powerpath = r"S:\ECT_LC\Elioth\20_Projets\BAOA129 - Bruneseau Equipe Hardel\21 - SmartGrid\2 - Calc\data\consommation\besoins\climelioth\out\ddfa09b8-4836-11ea-869a-40b0343cd3cf\climelioth.parquet"
    #IO from smartgrid
    #powerpath = r"S:\ECT_LC\Elioth\20_Projets\BAOA129 - Bruneseau Equipe Hardel\21 - SmartGrid\2 - Calc\data\reseau\exchanger_power\exchanger_power.csv"
    
    scenario = "base"
    paramfile = r"S:\ECT_LC\Elioth\20_Projets\BAOA129 - Bruneseau Equipe Hardel\21 - SmartGrid\2 - Calc\data\reseau\temperatures\in\1\base.json"
    # Saving output in the same file
    jsonpath = os.path.join(dir_path,"{}.json".format(scenario))
    path = path.format(scenario)
    dimfactor = 1.0
    Plot = False
    
    # Uncomment here to size on a specific datetime
    # datetime = "2005-01-30 07:00"
       

    try:
        opts, args = getopt.getopt(argv, "g:n:p:d:t:o:", ["geometry=","network=", "power=", "dimfactor=", "timebase=","output_json="])
    except getopt.GetoptError:
        print('run.py -g <network_geometry_directory> -n <network_temperatures_json> -p <power> -d <dimfactor> -t <datetime> -o <output_file_path>')
        sys.exit(2)
     
    for opt, arg in opts:
        if opt in ("-g", "--network_geometry"):
            path = arg
            Plot = False #default do not plot networkx when calling from command line
        if opt in ("-n", "--network_parameters"):
            paramfile = arg
        if opt in ("-p", "--power"):
            powerpath = arg
        if opt in ("-d", "--dimfactor"):
            dimfactor = float(arg)
        if opt in ("-t", "--datetime"):
            datetime = arg
        elif opt in ("-o", "--output"):
            jsonpath = arg
            
    #path = r"S:\ECT_LC\Elioth\20_Projets\BAOA129 - Bruneseau Equipe Hardel\21 - SmartGrid\2 - Calc\data\reseau\geometrie\traces\{}\\"
    #scenario = "0000-TRA_base"
    #path = path.format(scenario)
    powerpipes = pd.read_csv(os.path.join(dir_path,"data","power-pipes.csv"),index_col=0)
    wholenetwork = gpd.read_file(os.path.join(path,"reseau_georef.gpkg"))
    pdl = gpd.read_file(os.path.join(path,"pdl_georef.gpkg"))
    
    #jsonpath = "result.json"
    if powerpath.endswith(".parquet"): #Climelioth
        with open(powerpath, "rb") as f:
            power = pd.read_parquet(f)

    elif powerpath.endswith(".csv"): #IO from smartgrid
        power = geosmartgrid.parseIO(powerpath)

    loads = nt.getLoad(power,dimfactor=dimfactor,datetime=datetime)
    
                
    # Prepare data
    pdl = gpd.GeoDataFrame(pd.merge(loads,pdl, left_index=True,right_on="building_id"))
    
    with open(paramfile, 'r') as f:
        network_properties = json.loads(f.read())
        
#    network_properties={
#            "networks":{
#                "heating":{"Tin":47,"Tout":39},
#                "cooling":{"Tin":8,"Tout":15},
#                "medium-temperature":{"summer":{"Tin":30,"Tout":35},
#                           "winter":{"Tin":25,"Tout":20}
#                           }
#                },
#            "ground_type":"Argile humide",
#            "depth": 1.3
#            }
    
    
    networks = []
    for mode in list(network_properties["networks"].keys()):
        print("Mode: ", mode)
        if mode== "medium-temperature":
            seasons = ["summer","winter"]
        else:
            seasons = ["unique"]
            
        for season in seasons:
            print("Season: ", season)
            
            if mode == "heating":
                boucle = "chaud"
                source = "heating"
            elif mode == "medium-temperature":
                boucle = "temperee"
                if season == "winter":
                    source = "heating"
                elif season == "summer":
                    source = "cooling"
            elif mode == "cooling":
                boucle = "froid"
                source = "cooling"
                
            network = wholenetwork[wholenetwork.Source == boucle]
            network = network[network.Sens == "aller"]
            
            # Explode network
            network, points = nt.explode(network)
            network["season"] = season
            
            # Create exchangers
            exchangers = gpd.sjoin(points,pdl,how="left")
            exchangers[exchangers.columns.drop("geometry")] = exchangers[exchangers.columns.drop("geometry")].fillna(0)

            
            # Create network
            segments = nt.splitLine(network,exchangers, 0) #not really necessary anymore
            
            network = pd.merge(network,segments,left_index=True,right_on="oID")
            network = network.drop("geometry_x",axis=1)
            network = network.rename(columns={"geometry_y":"geometry"})
            network = network.reset_index()
            network = network.drop("index",axis=1)
            
            # Find intersections
            network = nt.findIntersections(network,exchangers)

            # Adding Building ID to network
            network["Origin"] = network.apply(lambda x: exchangers.loc[x.int1,"building_id"],axis=1)
            network["Destination"] = network.apply(lambda x: exchangers.loc[x.int2,"building_id"],axis=1)
        
            
            exchangers = exchangers.loc[set(list(network.int1.astype(int).unique())+list(network.int2.astype(int).unique())),:] #Points that are an actual intersection
            prodIDs = network[network.Origin !=0].Origin.unique() #building IDs with a heating/cooling station
            
            # Add production to heating and cooling stations
            if "Sousreseau" in network.columns:
                netnames = network.Sousreseau.unique()
                for net in netnames:
                    print("Subnetwork", net)
                    bldgIDs = network[(network.Sousreseau == net) & (network.Destination !=0)].Destination.tolist()
                    prodID = network[(network.Sousreseau == net) & (network.Origin !=0)].Origin.values[0]
                    bldgIDs.append(prodID)
                    exchangers.loc[exchangers.building_id == prodID,source] += -exchangers.loc[exchangers.building_id.isin(bldgIDs), source].sum().astype(int)
            else:
                if len(prodIDs) > 1:
                    raise ValueError('There is more than one production station: check your geometry')
                    sys.exit(1)
                exchangers.loc[exchangers.building_id == prodIDs[0],source] += -exchangers[source].sum().astype(int)
        
                # Check connected buildings
                origin = network.Origin[network.Origin != 0].unique().tolist()
                destinations = network.Destination[network.Destination != 0].unique().tolist()
                disconnected = power.building_id.unique()[~np.isin(power.building_id.unique(),origin+destinations)]
                print("")
                print("Building with thermal station:", ",".join(origin))
                print("Connected buildings:", ",".join(destinations))
                print("Disconnected buildings:", ",".join(disconnected))
                print("")
                
            # Create graph
            graph = nt.createGraph(network,exchangers,source)
            
            if Plot:
                import matplotlib.pyplot as plt
                fig, ax = plt.subplots() 
                pos=nx.get_node_attributes(graph,'pos')
                nx.draw_networkx(graph,pos=pos)
                
                fig, ax = plt.subplots() 
                nx.draw_networkx(graph)
            
            #Calculate flow
            flowCost, flowDict = nx.network_simplex(graph, demand = 'power',
                                                     capacity = 'capacity',
                                                     weight = 'length')
            network = nt.graphToPower(network, flowDict)
        
        
            # Select Pipes
            powerpipes.columns = powerpipes.columns.astype(int)
            # powerpipes = powerpipes.loc[:,pipesdata.DN]
            powerpipes = powerpipes.reindex(sorted(powerpipes.columns), axis=1)
            if mode == "medium-temperature":
                DeltaT = abs(network_properties["networks"][mode][season]["Tin"] - network_properties["networks"][mode][season]["Tout"])
            else:
                DeltaT = abs(network_properties["networks"][mode]["Tin"] - network_properties["networks"][mode]["Tout"])
            #print(network.power,DeltaT)
            network["DN"] = network.apply(lambda x: int(nt.getDN(x["power"]/1000,DeltaT,powerpipes)),axis=1)
            networks.append(network)
    
    networks = pd.concat(networks,ignore_index=True)
    networks = gpd.GeoDataFrame(networks)

    if "medium-temperature" in list(network_properties["networks"].keys()):
        print("Sizing medium-temperature network")
        summer = networks[networks.season =="summer"].copy().reset_index()
        winter = networks[networks.season =="winter"].copy().reset_index()
        maxseason = summer.where(summer.DN >= winter.DN, winter)
    
        networks = networks.drop(networks[networks.Source ==  "temperee"].index) #remove the two seasons
        networks = pd.concat([networks,maxseason],ignore_index=True, sort=False) #add the DN max of the two seasons

    #Saving Pipes
    #networks.to_csv("{}_{}.csv".format(scenario,source))
      
    with open(jsonpath,"w") as json_file:
        JSON = json.loads(networks.to_json())
        JSON["properties"] = network_properties
        json_file.write(json.dumps(JSON))
    
    scenario = os.path.basename(jsonpath).split(".")[0]

    lineaires.getLineaires(networks,os.path.dirname(jsonpath),scenario,path,Plot=True)   
    return(networks)
            
if __name__ == "__main__":
    main(sys.argv[1:])