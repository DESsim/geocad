import geopandas as gpd
import pandas as pd
import numpy as np
import math
from shapely.geometry import Polygon, LineString
import matplotlib.pyplot as plt


# Default Parameters
Tfluid = 8.5
TExt = 12
ground_type = "Argile humide"
depth = 1.3

                     
def ResistanceSoil(R,depth=depth,lambd=2):
    resistance = pd.Series()
    for i in range(R.shape[0]):
        resistance.loc[i] = (math.acosh((depth/R.loc[i])))/(2*math.pi * lambd)
    return resistance

def Resistance(r,R,lambd):
    resistance = pd.Series()
    for i in range(r.shape[0]):
        resistance.loc[i] = (math.log((R.loc[i]/r.loc[i])))/(2*math.pi * lambd.loc[i])
    return resistance

def linearLosses(pipes,weather="",Tfluid=8.5,TExt=12):
    hourlylosses = np.nan
    #Original Resistance considering air temperature
    pipes["coeff_losses"]  = 1/(Resistance(pipes.R1,pipes.R2,pipes.lambda_a)+Resistance(pipes.R2,pipes.R3,pipes.lambda_is)+ResistanceSoil(pipes.R3,depth=depth))
    pipes["length"] = pipes.geometry.length
    if len(weather) > 0:
        if "ground_temperature" in weather.columns:
            #Modified Resistance considering ground temperature
            pipes["coeff_losses"]  = 1/(Resistance(pipes.R1,pipes.R2,pipes.lambda_a)+Resistance(pipes.R2,pipes.R3,pipes.lambda_is))
            #pipes["annual_losses"] = pipes.apply(lambda x: ((x.length * x.coeff_losses * ((weather.ground_temperature-273.15) - Tfluid).abs()).sum(axis=0)),axis = 1)/1000
            hourlylosses = pd.DataFrame(index=weather.index,columns=pipes.index)
            for pipe in pipes.index:
                hourlylosses.loc[:,pipe] = (pipes.loc[pipe,"length"] * pipes.loc[pipe,"coeff_losses"] * ((weather.ground_temperature-273.15) - Tfluid).abs())/1000
                pipes.loc[pipe,"annual_losses"] = hourlylosses.loc[:,pipe].sum()
        else:
            pipes["annual_losses"] = pipes.apply(lambda x: ((x.length * x.coeff_losses * ((weather.air_temperature-273.15) - Tfluid).abs()).sum(axis=0)),axis = 1)/1000

    else:
        pipes["losses"]  = (pipes.length * pipes.coeff_losses * (TExt - Tfluid))/1000
        pipes["annual_losses"]  = (pipes.length * pipes.coeff_losses * 8760 * abs(TExt - Tfluid))/1000

    return pipes, hourlylosses

def compute_theta_g(weather, Z=depth, type_sol=ground_type):
    # Calcule la temp?rature de sol ? partir de :
    # Theta_a : temp?rature de l'air ext?rieur (8760 heures).
    # Z : la profondeur d'enfouissement des conduits.
    # Type_sol : conditionne la variabilit? annuelle de temp?rature (voir RT2012 p. 463)
    
    # To do : attraper les erreurs sur les inputs, v?rification des outputs, am?lioration plot (noms axes)
     
  Theta_a = weather.air_temperature-273.5

  # Valeurs de gm pour diff?rents sols (voir RT 2012 p. 463)
  gm_sols = pd.Series([1, 0.9, 0.98, 1.04, 1.05], index=["Sol humide", "Sable sec", "Sable humide", "Argile humide", "Argile mouillee"])

  # Moyenne annuelle
  Theta_AM = Theta_a.mean()

  # Moyenne des moyennes mensuelles 
  Theta_MM = Theta_a.resample("M").mean()

  # Demi-amplitude moyenne mensuelle maximale
  Delta_Theta_AM = (max(Theta_MM) - min(Theta_MM))/2

  AH = max(1-0.1993*Z + 0.01381*Z**2 - 0.000335*Z**3, 0)
  VS = 24*(0.1786 + 10.298*Z - 1.0156*Z**2 + 0.3385*Z**3 - 0.0195*Z**4)

  JH = np.array(range(1,8761))

  Theta_g = pd.DataFrame(columns=[["Value"]],index=JH)

  Theta_g.Value = Theta_g.apply(lambda x: gm_sols[type_sol] * (Theta_AM - AH * Delta_Theta_AM * math.sin(2*math.pi/8760*(x.name - VS + 24*25))),axis=1)
  Theta_g = pd.Series(np.array(Theta_g)[:,0],index=Theta_a.index,name="ground_temperature")

  return Theta_g

if __name__ == "__main__":
    path = r"S:\ECT_LC\Elioth\20_Projets\BAOA129 - Bruneseau Equipe Hardel\21 - SmartGrid\2 - Calc\data\\"
    DXF = gpd.read_file(path+"reseau/geometrie/reseau.dxf")
    pipesdata = pd.read_csv(path+"reseau/pertes/in/pipes.csv")
    weather = pd.read_csv(path+"meteo/out/Paris_Montsouris-hour.epw/Paris_Montsouris-hour.epw.csv",comment="#",index_col=0, parse_dates=True)
                          
    # Model for Ground temperature
    ground_temperature = compute_theta_g(weather,depth,ground_type)
    # Add ground temperature to weather
    weather["ground_temperature"] = ground_temperature+273.15
    
    # Import buildings
    buildings = DXF[DXF["Layer"].str.startswith("building")]
    buildings.rename(columns={'Layer':'Name'},inplace=True)
    buildings = buildings[["Name","geometry"]]
    
    # Import pipes
    reseau = DXF[DXF["Layer"].str.startswith("reseau")]
    reseau.rename(columns={'Layer':'Type'},inplace=True)
    reseau = reseau[["Type","geometry"]]
    reseau[["Type","DN"]] = reseau['Type'].str.split("$",expand=True)
    
    reseau["DN"] = reseau['DN'].str.replace("DN","")
    reseau["DN"] = reseau['DN'].astype(int)
    
    # Merge pipes geometry with data
    reseau = pd.merge(reseau,pipesdata,on="DN")
    
    # compute losses
    reseau, hourlylosses = linearLosses(reseau,weather,Tfluid=Tfluid)
    print("Annual losses",reseau.annual_losses.sum(), "kWh")
    hourlylosses.plot()
    plt.ylabel("Losses [kW]")
    plt.legend()
    plt.show()

