# -*- coding: utf-8 -*-
"""
Created on Fri Sep 27 14:53:59 2019

@author: g.peronato
"""
import networkx as nx
import geopandas as gpd
import matplotlib.pyplot as plt
from shapely.ops import split, snap, linemerge
from shapely.geometry import Point, LineString
from shapely import wkb
import pandas as pd
import numpy as np
import pyarrow.parquet as pq
import sys


def splitLine(gdf_line, gdf_points, tolerance=0.1):
    """
    Split the union of lines with the union of points resulting 
    Parameters
    ----------
    gdf_line : geoDataFrame
        geodataframe with multiple rows of lines
    gdf_points : geoDataFrame
        geodataframe with multiple rows of single points

    Returns
    -------
    gdf_segments : geoDataFrame
        geodataframe of segments
    """
    list_segments = []
    for i, line in gdf_line.iterrows():
        coords = gdf_points.geometry.unary_union
    
        # snap and split coords on line
        # returns GeometryCollection
        split_line = split(line.geometry, snap(coords, line.geometry, tolerance))
        
        # transform Geometry Collection to GeoDataFrame
        segments = [feature for feature in split_line]
        
        # merge the first and last segment of a ring
        # if there is more than one segment
        # if the first vertex is not a splitting point
        if len(segments) > 1 and not Point(segments[0].coords[0]) in gdf_points.geometry:
            segments = [linemerge([segments[-1],segments[0]])] + segments[1:-1]

        # create a GeoDataFrame from the list of segments and the set the original ID
        gdf_segments = gpd.GeoDataFrame(list(range(len(segments))), geometry=segments)
        gdf_segments["oID"] = i
        list_segments.append(gdf_segments)

    segments = pd.concat(list_segments)
    segments = segments.rename(columns={0: "segID"})

    return segments

def explode(lines):
    exploded = []
    points = []
    for l, line in lines.iterrows():
        #print(len(line.geometry.coords))
        for p in range(len(line.geometry.coords)-1):
            segment = line.copy()
            segment.name = 0
            segment["geometry"] = LineString([line.geometry.coords[p],line.geometry.coords[p+1]])
            points.append(Point(line.geometry.coords[p]))
            points.append(Point(line.geometry.coords[p+1]))
            exploded.append(gpd.GeoDataFrame(segment))
    exploded = gpd.GeoDataFrame(pd.concat(exploded,axis=1,ignore_index=True).transpose())
    points = gpd.GeoSeries(points)
    pointsDF = pd.DataFrame()
    pointsDF["geometry"] = points
    pointsDF["geometry"] = pointsDF["geometry"].apply(lambda geom: geom.wkb)
    pointsDF = pointsDF.drop_duplicates(["geometry"])
    pointsDF["geometry"] = pointsDF["geometry"].apply(lambda geom: wkb.loads(geom))
    pointsDF = gpd.GeoDataFrame(pointsDF)
    return exploded, pointsDF
            
def findIntersections(network,points):
    network["int1"] = np.nan
    network["int2"] = np.nan
    for s, segment in network.iterrows():
        for vertex in segment.geometry.coords:
            for p, point in points.iterrows():
                if vertex == point.geometry.coords[0]:
                    if not np.isnan(network.loc[s,"int1"]):
                        network.loc[s,"int2"] = p
                    else:
                        network.loc[s,"int1"] = p
    if network[["int1","int2"]].isnull().values.any():
        print("Warning: every network segment must have at least two intersecting points")
    return network

def createGraph(network,points,source="power"):
    G = nx.DiGraph()
    for n, node in points.iterrows():
        G.add_node(str(int(n)),power = int(node[source]), pos = node.geometry.coords[0])
        # print(n,int(node[source]))
    for l in network.oID.unique():
        segments = network[network.oID == l]
        for s, segment in segments.iterrows():
            if not np.isnan(segment.int2):
                G.add_edge(str(int(segment.int2)), str(int(segment.int1)), length=int(segment.geometry.length))
                # print(str(int(segment.int2)), str(int(segment.int1)), int(segment.geometry.length))

    return G

def graphToPower(network, flowdict):
    network["power"] = np.nan
    flowdict = pd.DataFrame(flowdict)
    for s, segment in network.iterrows():
        network.loc[s,"power"] = flowdict.loc[str(int(segment.int1)),str(int(segment.int2))]
    return network

def getLoad(power,PDL="",dimfactor=1.0,datetime=""):
    power = power.set_index("timebase")
    loads = pd.DataFrame(index=power.index)
    if "p_heat_reco" in power.columns: #check if this is a climelioth imput
        loads["heating"] = (power.p_heat + power.p_hw - power.p_heat_reco)
        loads["cooling"] = (-power.p_cool + power.p_cool_process - power.p_cool_reco)
        loads["building_id"] = power.building_id
    else:
        loads = power

        
#    peakh = loads.resample("h").sum().idxmax()
#    maxloads = pd.DataFrame()
#    maxloads["heating"] = -loads[str(peakh.heating)].groupby("building_id").sum().heating.astype(int)
#    maxloads["cooling"] = -loads[str(peakh.cooling)].groupby("building_id").sum().cooling.astype(int)
    if len(datetime) > 0:
        print("Sizing with loads on {}".format(datetime))
        load = -loads.groupby(["building_id","timebase"]).sum()
        load = load.reset_index()
        load[["heating","cooling"]] = load[["heating","cooling"]].astype(int)
        load = load[load.timebase == datetime]
        load = load.drop(["timebase"],axis=1)
        load = load.set_index("building_id")
    else:
        print("Sizing with max load per each building")
        load = -loads.groupby(["building_id","timebase"]).sum().droplevel(1).groupby("building_id").max().astype(int)
    return load

def getDN(power,DeltaT,powerpipes):
    idx = powerpipes.columns.get_loc(abs((powerpipes.loc[DeltaT,:])-power).idxmin())+1
    return int(powerpipes.columns[idx])

# Code to debug
if __name__ == "__main__":
    try:
        script_path = os.path.abspath(__file__)
        dev_path = os.path.dirname(os.path.dirname(script_path)) #"D:\DATA\g.peronato\dev\"
        sys.path.append(os.path.join(dev_path,"smartgrid\python\smartgrid"))
        import losses
    except:
        print("Smartgrid package not found")
    # Import data
    path = "sampledata/"
#    network = gpd.read_file(path+"reseau_georef.gpkg")
#    exchangers = gpd.read_file(path+"soustations_georef.gpkg")
#    pdl = gpd.read_file(path+"pdl_georef.gpkg")
#    buildings = gpd.read_file(path+"volumes_georef.gpkg")
    path = r"S:\ECT_LC\Elioth\20_Projets\BAOA129 - Bruneseau Equipe Hardel\21 - SmartGrid\2 - Calc\data\reseau\geometrie\traces\{}\\"
    scenario = "2010-TRA_nord_sud_V1"
    path = path.format(scenario)
    network = gpd.read_file(path+"reseau_georef.gpkg")
    #exchangers = gpd.read_file(path+"soustations.gpkg")
    pdl = gpd.read_file(path+"pdl_georef.gpkg")
    buildings = gpd.read_file(path+"volumes_georef.gpkg")
    
    path = "sampledata/"
    loads = getLoad(pq.read_table(path+"all.parquet").to_pandas())
    pipesdata = pd.read_csv(path+"pipes.csv")
    powerpipes = pd.read_csv(path+"power-pipes.csv",index_col=0)
    weather = pd.read_csv(path+"H1a.csv",comment="#",index_col=0, parse_dates=True)
                          
    source = "cooling"
    prodID = "B1C1.logements"
    Tin = 47
    Tout = 39
    ground_type = "Argile humide"
    depth = 1.3

    # Prepare data
    pdl = pd.merge(loads,pdl, left_index=True,right_on="building_id")
#    exchangers = pd.concat([exchangers,pdl], ignore_index=True, sort=True)
#    exchangers = exchangers.fillna(0)
    exchangers = gpd.GeoDataFrame(pdl.copy())

    
    network = network[network.Source == "chaud"]
    network = network[network.Sens == "aller"]
    
    #Explode network
    network, points = explode(network)
    
    #Create exchangers
    exchangers = gpd.sjoin(points,exchangers,how="left")
    exchangers = exchangers.fillna(0)
    
    # Create network
    segments = splitLine(network,exchangers, 0) #not really necessary
    
    network = pd.merge(network,segments,left_index=True,right_on="oID")
    network = network.drop("geometry_x",axis=1)
    network = network.rename(columns={"geometry_y":"geometry"})
    network = network.reset_index()
    network = network.drop("index",axis=1)
    
    network = findIntersections(network,exchangers)
    #Adding Building ID to network
    network["Origin"] = network.apply(lambda x: exchangers.loc[x.int1,"building_id"],axis=1)
    network["PDL"] = network.apply(lambda x: exchangers.loc[x.int2,"building_id"],axis=1)

    # Select data
    #network = network[network.Source == "chaleur"]
    exchangers = exchangers.loc[set(list(network.int1.astype(int).unique())+list(network.int2.astype(int).unique())),:]
    exchangers.loc[exchangers.building_id == prodID,source] += -exchangers[source].sum().astype(int)
    
    # Plotting network
    fig, ax = plt.subplots() 
    #network.plot(ax=ax,column="oID",linewidth=10)
    buildings.plot(ax=ax,color="grey")
    network.plot(ax=ax,column="Source",linewidth=3)
    exchangers[exchangers[source]>0].plot(ax=ax,color="red",markersize=exchangers[exchangers[source]>0][source].abs()/10000)
    exchangers[exchangers[source]<0].plot(ax=ax,color="yellow",markersize=exchangers[exchangers[source]<0][source].abs()/10000)
    exchangers.apply(lambda x: ax.annotate(s=x.name, xy=x.geometry.coords[0], ha='center', color="k"),axis=1)
    #network.apply(lambda x: ax.annotate(s=x.segID, xy=x.geometry.coords[1], ha='center'),axis=1);
    
    # Create graph  
    graph = createGraph(network,exchangers,source)
    fig, ax = plt.subplots() 
    pos=nx.get_node_attributes(graph,'pos')
    nx.draw_networkx(graph,pos=pos)
    
    fig, ax = plt.subplots() 
    nx.draw_networkx(graph)
    
    #Calculate flow
    flowCost, flowDict = nx.network_simplex(graph, demand = 'power',
                                             capacity = 'capacity',
                                             weight = 'length')

    network = graphToPower(network, flowDict)

    # Plotting flows
    fig, ax = plt.subplots() 
    buildings.plot(ax=ax,color="grey")
    network.plot(ax=ax,color="grey")
    network.plot(ax=ax,color="black",linewidth=network.power/100000)
    exchangers.apply(lambda x: ax.annotate(s=x.name, xy=x.geometry.coords[0], ha='center', color="k"),axis=1)

    # Select Pipes
    powerpipes.columns = powerpipes.columns.astype(int)
    powerpipes = powerpipes.loc[:,pipesdata.DN]
    powerpipes = powerpipes.reindex(sorted(powerpipes.columns), axis=1)

    DeltaT = Tin - Tout
    network["DN"] = network.apply(lambda x: int(getDN(x["power"]/1000,DeltaT,powerpipes)),axis=1)
 
    # Plotting DN  
    fig, ax = plt.subplots() 
    buildings.plot(ax=ax,color="grey")
    #network["DN"] = network.apply(lambda x: str(x.DN),axis=1)
    network.plot(ax=ax,column="DN",legend=True, linewidth=2)
    plt.title("DN")
    
    #Saving Pipes
    network.to_csv("{}_{}.csv".format(scenario,source))
    jsonpath = "{}_{}.json".format(scenario,source)
    #network.to_file(jsonpath, driver="GeoJSON")
    
    import json
    temperatures = {"Tin": Tin, "Tout": Tout}
    with open(jsonpath,"w") as json_file:
        JSON = json.loads(network.to_json())
        JSON["properties"] = temperatures
        json_file.write(json.dumps(JSON))
        #json_file.write(json.dumps({"Properties": temperatures}))


    # Compute Losses
    ground_temperature = losses.compute_theta_g(weather,depth,ground_type)
    weather["ground_temperature"] = ground_temperature+273.15
    network = pd.merge(network,pipesdata,on="DN")
    network["Tfluid"] = (Tin+Tout)/2
    reseau, hourlylosses = losses.linearLosses(network,weather)
    
    # Plotting results  
    fig, ax = plt.subplots() 
    buildings.plot(ax=ax,color="grey")
    network["normalized_losses"] = network.annual_losses/ network.length
    network.plot(ax=ax,column="normalized_losses",legend=True, linewidth=2)
    plt.title("Losses [kWh/m]")
    
    # Compute stats
    needs = pq.read_table(path+"all.parquet").to_pandas()
    needs["heating"] = needs.p_heat + needs.p_hw + needs.p_swim - needs.p_heat_reco

    connected = exchangers.building_id[exchangers.building_id != 0].tolist()
    needs = needs[needs.building_id.isin(connected)]
    loads = loads[loads.index.isin(connected)]

    print("Pertes consos", network.annual_losses.sum()/(needs.heating.sum()/1000)*100,"%")
    print("Pertes consos", network.annual_losses.sum()/1000,"MWh")
    print("Pertes puissance", hourlylosses.sum(axis=1).mean()/(-loads.sum().heating/1000)*100,"%")
    print("Puissance moyenne", hourlylosses.sum(axis=1).mean(),"kW")
    
    print(network.groupby("DN").sum()["length"].round().astype(int))
    

    network.crs = {'init' :'epsg:2154'}

